import calculator.Arithmetic;
import games.Games;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    static BufferedReader reader = new BufferedReader (new InputStreamReader(System.in));

    public static void main (String[] args) throws IOException {
        for (int i = 0; i < 14; i++)
            System.out.print("*--*");
        System.out.print ("\n" + "Welcome to the program with games and arithmetic operations." + "\n");
        for (int i = 0; i < 14; i++)
            System.out.print("*--*");
        System.out.println ("\n");
        while (true) {
            System.out.println("Please choose the option:" + "\n" +
                    "1 - games program which includes matrix creation and checking if the word is palindrome" + "\n" +
                    "2 - providing arithmetic operations, such as multiplication, division, power and root" + "\n" +
                    "3 - exit the program");
            String choice = reader.readLine();
            switch (choice) {
                case "1": {
                    Games.gamesMenu();
                    break;
                }
                case "2": {
                    Arithmetic.arithmeticMenu();
                    break;
                }
                case "3": {
                    System.out.println("Thank you for using this program.");
                    System.exit(1);
                }
                default: {
                    System.out.println("Wrong choice. Please try again:");
                }
            }
        }
    }
}