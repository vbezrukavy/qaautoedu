package calculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Arithmetic {

    static BufferedReader reader = new BufferedReader (new InputStreamReader(System.in));

    public static void arithmeticMenu () throws IOException {
        for (int i = 0; i < 14; i++)
            System.out.print("*--*");
        System.out.print ("\n" + "Welcome to the program with arithmetic operations." + "\n");
        for (int i = 0; i < 14; i++)
            System.out.print("*--*");
        System.out.println ("\n");

        while (true) {
            System.out.println ("Please choose the operation which you want to provide:" + "\n" +
                    "1 - multiplication of numbers from input array;" + "\n" +
                    "2 - power of input number" + "\n" +
                    "3 - division of two input numbers" + "\n" +
                    "4 - root of input number" + "\n");
            String choice = reader.readLine();
            switch (choice) {
                case "1": {
                    System.out.println("Please enter the array of numbers with spaces between:");
                    double[] arrayNumbers = Arithmetic.returnArray();
                    double resultMult = Arithmetic.arrayMultiplication(arrayNumbers);
                    System.out.println ("Multiplication of all input numbers = " + resultMult);
                    break;
                }
                case "2": {
                    System.out.println("Please enter the number which you want to raise in power:");
                    double number = Arithmetic.returnInputDoubleValue();
                    System.out.println("Please enter the power to raise:");
                    int power = Arithmetic.returnInputIntValue();

                    double resultPow = Arithmetic.power(number, power);
                    System.out.println (number + " in " + power + " power is equal to " + resultPow);
                    break;
                }
                case "3": {
                    System.out.println("Please enter the dividend:");
                    double dividend = Arithmetic.returnInputDoubleValue();
                    System.out.println("Please enter the divider:");
                    double divider = Arithmetic.returnInputDoubleValue();
                    double resultDiv;
                    if (divider == 0) {
                        double newDivider;
                        do {
                            System.out.println("Please enter the divider which is not 0:");
                            newDivider = Arithmetic.returnInputDoubleValue();
                        }
                        while (newDivider == 0);
                        resultDiv = Arithmetic.division(dividend, newDivider);
                        System.out.println("Division of " + dividend + " and " + newDivider + " is equal to " + resultDiv);
                    } else {
                        resultDiv = Arithmetic.division(dividend, divider);
                        System.out.println("Division of " + dividend + " and " + divider + " is equal to " + resultDiv);
                    }
                    break;
                }
                case "4": {
                    System.out.println("Please enter the number to get the root of it:");
                    double number = Arithmetic.returnInputDoubleValue();
                    double resultRoot = 0;
                    if (number < 0) {
                        double newNumber = 0;
                        do {
                            System.out.println("Please enter the number, which is bigger than 0:");
                            newNumber = Arithmetic.returnInputDoubleValue();
                        }
                        while (newNumber < 0);
                        resultRoot = Arithmetic.root(newNumber);
                        System.out.println ("Root of " + newNumber + " is " + resultRoot);
                    } else {
                        resultRoot = Arithmetic.root(number);
                        System.out.println("Root of " + number + " is " + resultRoot);
                    }
                    break;

                }
                default: {
                    System.out.println("Wrong choice. Please try again.");
                    arithmeticMenu();
                }
            }
            System.out.println("If you want to continue using this program press Y/YES, if you want to exit to the menu - press any key:");
            String inputChoice = reader.readLine();

            if (inputChoice.equalsIgnoreCase("Y") || inputChoice.equalsIgnoreCase("YES"))
                continue;
            else
                break;
        }
    }

    public static double returnInputDoubleValue () {
        while (true) {
            try {
                String inputString = reader.readLine();
                double inputValue = Double.parseDouble(inputString);
                return inputValue;
            }
            catch (Exception e) {
                System.out.println("You have entered an incorrect value. Please enter any number:");
            }
            continue;
        }
    }

    public static double[] returnArray () {
        while (true) {
            try {
                String[] arrayString;
                String inputString = reader.readLine();
                arrayString = inputString.split(" ");
                double[] arrayNumbers = new double[arrayString.length];

                for (int i = 0; i != arrayString.length; i++) {
                    arrayNumbers[i] = Double.parseDouble(arrayString[i]);
                }
                return arrayNumbers;
            }
            catch (Exception e) {
                System.out.println("You should enter the array of numbers with one space between. Please try again:");
            }
        }
    }

    public static int returnInputIntValue () throws IOException {
        while (true) {
            try {
                String inputString = reader.readLine();
                int inputValue = Integer.parseInt(inputString);
                return inputValue;
            } catch (Exception e) {
                System.out.println("You have entered an incorrect value. Please enter any integer:");
            }
            continue;
        }
    }

    public static double arrayMultiplication (double[] mass) {
        double resultMult = 1;
        for (int i = 0; i < mass.length; i++) {
            resultMult = resultMult * mass [i];
        }

        System.out.print ("Input array is:" + "\n" +
                "{");

        for (int i = 0; i < mass.length-1; i++) {
            System.out.print (mass[i] + ", ");
        }
        System.out.println (mass[mass.length-1] + "}");
        return resultMult;
    }

    public static double power (double number, int power) {
        double resultPow = 0;
        resultPow = Math.pow (number, power);
        return resultPow;
    }

    public static double division (double dividend, double divider) {
        double resultDiv = 0;
        resultDiv = dividend / divider;
        return resultDiv;
    }

    public static double root (double number) {
        double resultRoot = 0;
        resultRoot = Math.sqrt (number);
        return resultRoot;
    }
}