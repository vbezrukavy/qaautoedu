package games;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Games {
    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    public static void gamesMenu () throws IOException {
        for (int i = 0; i < 14; i++)
            System.out.print("*--*");
        System.out.print ("\n" + "Welcome! You have entered the program with games." + "\n");
        for (int i = 0; i < 14; i++)
            System.out.print("*--*");
        System.out.println ("\n");

        while (true) {
            System.out.println("Please choose the option which you want to provide:" + "\n" +
                    "1 - matrix creation" + "\n" +
                    "2 - matrix creation using snail algorithm" + "\n" +
                    "3 - checking if the word is palindrome");
            String choice = reader.readLine();
            switch (choice) {
                case "1": {
                    Matrix.matrixMenu();
                    break;
                }
                case "2": {
                    while (true) {
                        int size = Snail.enterSizeMatrix ();
                        int matrix[][] = Snail.snailMatrix (size);
                        Matrix.printMatrix(matrix, size);

                        System.out.println("If you want to continue press Y/YES, if you want to exit - press any key:");
                        String data = reader.readLine();
                        if (data.equalsIgnoreCase("Y") || data.equalsIgnoreCase("YES"))
                            continue;
                        else {
                            System.out.println("Thank you for using this program.");
                            System.exit(1);
                        }
                        break;
                    }
                }
                case "3": {
                    String inputString = Palindrome.palindromeMenu();
                    if (Palindrome.isPalindromePhrase(inputString) == true) {
                        System.out.println("The word " + inputString + " is palindrome.");
                        break;
                    }
                    else {
                        System.out.println("The word " + inputString + " is not a palindrome.");
                        break;
                    }
                }
                default: {
                    System.out.println("Wrong choice. Please try again:");
                    gamesMenu();
                }
            }
            System.out.println ("If you want to continue using this program press Y/YES, if you want to exit to the menu - press any key:");
            String inputString = reader.readLine ();
            if (inputString.equalsIgnoreCase ("Y") || inputString.equalsIgnoreCase ("YES"))
                continue;
            else
                break;

        }
    }
}