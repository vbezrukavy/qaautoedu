package games;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Snail {
    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static int enterSizeMatrix  () throws IOException {
        System.out.println("Enter any size of matrix > 3:");
        String inputString = reader.readLine();
        int size = Integer.parseInt(inputString);
        if (size < 3) {
            System.out.println ("You have entered wrong value. Please try again:");
            enterSizeMatrix ();
        }
        return size;
    }

    public static int[][] snailMatrix (int n) throws IOException {
        int[][] m = new int[n][n];
        int i, j;

        if (n % 2 == 0) {
            i = n / 2 - 1;
            j = n / 2 - 1;
        } else {
            i = n / 2;
            j = n / 2;
        }
        // задаем границы движения
        int min_i = i;          // вверх вниз
        int max_i = i;
        int min_j = j;          // влево вправо
        int max_j = j;
        int d = 0; // сначала пойдем вправо
        for (int a = 1; a <= n * n; a++) {
            m[i][j] = a;
            switch (d) {
                case 0:
                    j += 1;  // движение вправо
                    if (j > max_j) { // проверка выхода за заполненную центральную часть справа
                        d = 1; // меняем направление
                        max_j = j; // увеличиваем заполненную часть вправо
                    }
                    break;
                case 1:  // движение вниз проверка снизу
                    i += 1;
                    if (i > max_i) {
                        d = 2;
                        max_i = i;
                    }
                    break;
                case 2:  // движение влево проверка слева
                    j -= 1;
                    if (j < min_j) {
                        d = 3;
                        min_j = j;
                    }
                    break;
                case 3:  // движение вверх проверка сверху
                    i -= 1;
                    if (i < min_i) {
                        d = 0;
                        min_i = i;
                    }
            }
        }
        return m;
    }
}
