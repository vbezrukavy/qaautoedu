package games;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Palindrome {
    static BufferedReader reader = new BufferedReader (new InputStreamReader(System.in));
    public static String palindromeMenu() throws IOException {
        while (true) {
            System.out.println("Please enter a phrase to check:");
            String data = reader.readLine();
            boolean p = isPalindromePhrase (data);
            if (p == true)
                System.out.println("The phrase " + data + " is palindrome.");
            else
                System.out.println("The phrase " + data + " is not palindrome.");
            System.out.println("If you want to continue press Y/YES, if you want to exit - press any key:");
            String data1 = reader.readLine();
            if (data1.equalsIgnoreCase("Y") || data1.equalsIgnoreCase("YES"))
                continue;
            else {
                System.out.println("Thank you for using this program.");
                System.exit(1);
            }
        }
    }

    public static boolean isPalindromePhrase (String data) {
        data = data.toLowerCase();
        data = data.replace(" ", "");
        int dataLen = data.length();

        for(int i = 0; i < dataLen / 2; i++)
            if(data.charAt(i) != data.charAt(dataLen - i - 1))
                return false;
        return true;
    }
}
