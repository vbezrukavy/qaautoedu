package games;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Matrix {

    static BufferedReader reader = new BufferedReader (new InputStreamReader(System.in));
    public static void matrixMenu () throws IOException {

        int matrix [][];
        while (true){
            System.out.println ("Enter any value from 1 to 9:");
            String inputString = reader.readLine();

            if (Matrix.matchString (inputString)) {
                int value = Integer.parseInt (inputString);

                matrix = Matrix.createMatrix (value);
                Matrix.printMatrix (matrix, value);
            }
            else {
                System.out.println ("You have entered wrong value. Please try again:");
                matrixMenu();
            }
            break;
        }
    }

    public static boolean matchString (String s){
        Pattern p = Pattern.compile ("^1|2|3|4|5|6|7|8|9$");
        Matcher m = p.matcher (s);
        return m.matches ();
    }

    public static int[][] createMatrix (int size) {
        System.out.println ("check");
        int[][] matrix = new int[size][size];
        int digit = 0;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                digit = fillMatrix(digit);
                matrix[i][j] = digit;
            }
        }
        return matrix;
    }

    public static int fillMatrix (int digit) {
        if (digit == 9) {
            digit = 1;
        } else {
            digit++;
        }
        return digit;
    }

    public static void printMatrix (int [][] matrix, int size) {
        System.out.print ("Matrix is:" + "\n");
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print (matrix[i][j] + "\t");
            }
            System.out.println ();
        }
    }
}