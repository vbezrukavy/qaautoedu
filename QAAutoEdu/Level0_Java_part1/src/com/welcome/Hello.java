package com.welcome;

/**
 * Created by bezrukavy on 29.11.2016.
 */
public class Hello {
    private String stringName = "";
    public void setupName (String name) {
        this.stringName = name;
    }

    public void welcome () {
        System.out.println ("Hello, " +  stringName);
    }

    public void byeBye () {
        System.out.println ("Bye, " +  stringName);
    }
}
