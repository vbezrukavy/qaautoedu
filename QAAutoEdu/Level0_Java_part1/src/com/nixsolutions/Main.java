package com.nixsolutions;
import com.welcome.Hello;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Hello hello = new Hello ();

        System.out.println ("Please enter your name:");

        Scanner scanner = new Scanner(System.in);
        String stringName = scanner.next();

        hello.setupName (stringName);
        hello.welcome();

        System.out.println ("Hello, world!");

        hello.byeBye();
    }
}
